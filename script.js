
let trainer = {
	name: 'Ash Ketchum',
	age: 18,
		friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
		},
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	talk: function () {
		console.log('Pikachu! I choose you!')
	}
};

console.log(trainer);
console.log('Result of dot notation:')
console.log(trainer.name);
console.log('Result of square bracket notation:');
console.log(trainer.pokemon);
console.log('Result of talk method:');
trainer.talk();

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;

	this.tackle = function(person1, person2) {
		console.log(person1.name + ' tackled ' + person2.name );
	}
	this.faint = function (person2) {
		console.log(person2.name + ' fainted');
	}
}

let pokemon1 = new Pokemon('Pikachu', 12);
console.log(pokemon1);


let pokemon2 = new Pokemon('Geodude', 8);
console.log(pokemon2);

let pokemon3 = new Pokemon('Meowtwo', 100);
console.log(pokemon3);

pokemon1.tackle(pokemon2, pokemon1);
pokemon1.tackle(pokemon3, pokemon2);

pokemon1.faint(pokemon3);